#include <stdio.h>
#include <stdlib.h>
#include "colapri.h"
#include "colapri-lista.h"

/* La idea principal de una colapri es que no se sabe que' almacena la
 * lista.  Podria almacenar strings, punteros a personas, punteros a
 * longs e incluso longs, pero el codigo de este archivo no cambia.
 *
 * Lo unico que se requiere es que el que cree la cola de prioridades
 * con nuevaColaPri debe suministrar una funcion 'comp' que sirva para
 * comparar prioridades.
 */

struct colaprilist {
  ColaPriOps *ops;
  CompPri comp;
  int n;
  void *elem;
  struct colaprilist *sig;
};

typedef struct colaprilist ColaPriList; /* ColaPriList no es un puntero, ColaPri si lo es */

static void agregar(ColaPri colapri, void *a);
static void* extraer(ColaPri colapri);
static void* mejor(ColaPri colapri);
static int tamano(ColaPri colapri);
static void destruir(ColaPri colapri); /* debe estar vacia */
static IterColaP iterador(ColaPri colapri);


/*******************************************
 * Implementacion de la cola de prioridades
 *******************************************/
static ColaPriOps colapri_list_ops= {
  agregar, extraer, mejor, tamano, destruir, iterador
};

ColaPri nuevaColaPriLista(CompPri comp) {
  ColaPriList *cp= malloc(sizeof(ColaPriList));
  if (cp == NULL) {
    fprintf(stderr, "Error! No hay memoria!\n");
    exit(-1);
  }
  cp->ops= &colapri_list_ops;
  cp->comp= comp;
  cp->elem= NULL;
  cp->sig= NULL;
  cp->n = 0;
  return (ColaPri)cp;
}

/* Los elementos estan ordenados de mejor a peor prioridad */
static void agregar(ColaPri colapri, void *a) {
  ColaPriList *cp= (ColaPriList*)colapri;
  while (cp->sig != NULL && cp->sig->elem != NULL) {
    if ((*cp->comp)(a, cp->sig->elem)>=0) {
      cp->sig->n=cp->n;
      cp=cp->sig;
    } else {
      break;
    }
  }
  ColaPriList *nuevo = (ColaPriList*)nuevaColaPriLista(cp->comp);
  nuevo->sig = cp->sig;
  cp->sig = nuevo;
  if (cp->elem != NULL) {
    if ((*cp->comp)(cp->elem, a)<0)
      nuevo->elem = a;
    else {
      void *aux = cp->elem;
      cp->elem = a;
      nuevo->elem = aux;
    }
  } else {
    cp->elem = a;
  }
  cp->n++;
  ((ColaPriList*)colapri)->n = cp->n;
}

static void* mejor(ColaPri colapri) {
  ColaPriList *cp= (ColaPriList*)colapri;
  return cp->elem;
}

static int tamano(ColaPri colapri) {
  ColaPriList *cp= (ColaPriList*)colapri;
  return cp->n;
}

static void* extraer(ColaPri colapri) {
  ColaPriList *cp= (ColaPriList*)colapri;
  void *res= cp->elem;
  ColaPriList *c2 = cp->sig;
  cp->sig->n = cp->n;
  cp->elem = cp->sig->elem;
  cp->sig = cp->sig->sig;
  free(c2);
  cp->n--;
  return res;
}

static void destruir(ColaPri colapri) {
  ColaPriList *cp1= (ColaPriList*)colapri;
  ColaPriList *cp2;
  while (cp1->elem != NULL) {
    cp2 = cp1->sig;
    free(cp1);
    cp1 = cp2;
  }
  free(cp1);
}

/*******************************************
 * Implementacion de la cola de prioridades
 *******************************************/

typedef struct {
  IterColaPOps *ops;
  ColaPriList *cp;
  int k;
} IterColaPList;

static int continua(IterColaP iter);
static void* proximo(IterColaP iter);
static void destruirIter(IterColaP iter);

static IterColaPOps iterOps= { continua, proximo, destruirIter };

IterColaP iterador(ColaPri colapri) {
  IterColaPList *iter= malloc(sizeof(IterColaPList));
  if (iter == NULL) {
    fprintf(stderr, "Error! No hay memoria!\n");
    exit(-1);
  }
  iter->ops= &iterOps;
  iter->cp= (ColaPriList*)colapri;
  return (IterColaP)iter;
}

static int continua(IterColaP itercp) {
  IterColaPList *iter= (IterColaPList*)itercp;
  return iter->cp->elem != NULL;
}

static void *proximo(IterColaP itercp) {
  IterColaPList *iter= (IterColaPList*)itercp;
  ColaPriList* aux = iter->cp;
  iter->cp = iter->cp->sig;
  return aux->elem;
}

static void destruirIter(IterColaP iter) {
  free(iter);
}
